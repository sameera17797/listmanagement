package com.example.list;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class search2 extends AppCompatActivity {


    ListView listUnits;
    FirebaseDatabase database;
    DatabaseReference ref;
    ArrayList<String> li;
    ArrayAdapter <String> adp;
    List sheets;
    Spinner col;
    String selected ;
    Button search;
    int inputLength ;
    int inputWidth ;
    EditText inputl , inputw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search2);
        listUnits = (ListView) findViewById(R.id.my);
        search = (Button) findViewById(R.id.searchButton);
        col = (Spinner) findViewById(R.id.spinner1);
        inputl = (EditText) findViewById(R.id.inputl);
        inputw = (EditText) findViewById(R.id.inputW);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                 inputLength = Integer.parseInt(inputl.getText().toString().trim());
                 inputWidth  = Integer.parseInt(inputw.getText().toString().trim());



                sheets = new List();

                database = FirebaseDatabase.getInstance();
                col.setOnItemSelectedListener(new CustomSelectedListener());
                selected = String.valueOf(col.getSelectedItem());
                ref = database.getReference();
                li = new ArrayList<>();



                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot ds :dataSnapshot.getChildren())
                        {

                            List list = ds.getValue(List.class);
                            if(list.getColor().equals(selected) && list.getWid()>inputWidth && list.getLen()>inputLength )
                            {


                                li.add("L: " + list.getLen() + " W: " + list.getWid() + " SKU: " + list.getSku());

                            }


                        }
                        adp = new ArrayAdapter<String>(search2.this , R.layout.list_info , R.id.dis_col , li);
                        listUnits.setAdapter(adp);


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

        });



    }
}
