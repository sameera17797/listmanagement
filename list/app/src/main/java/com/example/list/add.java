package com.example.list;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class add extends AppCompatActivity {
    EditText length , width ;
    Spinner colour ;
    FirebaseDatabase database;
    DatabaseReference reff , getSku;
    List li , bt;
    ListView bluetoothList;
    TextView  service;
    Button add , turnON;
    Button back ;

    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket bluetoothSocket;
    BluetoothDevice bluetoothDevice;
    OutputStream outputStream;
    InputStream inputStream;
    Thread thread;
    ArrayAdapter<String> B_Devices;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    private static final int REQUEST_ENABLE_BT = 0;
    private static final int REQUEST_DISCOVER_BT = 0;
    
    int j;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);


        length = (EditText) findViewById(R.id.editText);
        width = (EditText) findViewById(R.id.editText5);
        colour = (Spinner) findViewById(R.id.spinner_color);
        colour.setOnItemSelectedListener(new CustomSelectedListener());
        add = (Button) findViewById(R.id.button2);
        back = (Button) findViewById(R.id.button4) ;
        service = (TextView) findViewById(R.id.printerStatus);
        turnON = (Button) findViewById(R.id.turnOnBluetooth);
        bluetoothList = (ListView) findViewById(R.id.mbluetoothList);
        int k = getcheck();

        reff = FirebaseDatabase.getInstance().getReference();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        openBT();  //Open Bluetooth
        openBluetoothPrinter(); // COnccect to the printer

        add.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {



                int getLastSKU = getcheck(); //SKU code request
                int inputWidth = Integer.parseInt(width.getText().toString().trim());
                int inputLength = Integer.parseInt(length.getText().toString().trim());
                String convertedSKU = Integer.toString(getLastSKU);
                try {
                    sendData(convertedSKU ,inputWidth ,inputLength , String.valueOf(colour.getSelectedItemId()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                List li = new List(inputWidth, inputLength, convertedSKU, String.valueOf(colour.getSelectedItem()));

                reff.push().setValue(li);

                       Toast.makeText(add.this, "Item saved "+convertedSKU+"", Toast.LENGTH_LONG).show();

            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(add.this , homePage.class );
                startActivity(home);
            }
        });




        turnON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!bluetoothAdapter.isEnabled()){
                    Toast.makeText(add.this, "Turning On Bluetooth...", Toast.LENGTH_SHORT).show();
                    //intent to on bluetooth
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, REQUEST_ENABLE_BT);
                }
                else {
                    Toast.makeText(add.this, "Bluetooth is Already On", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    void openBT()
    {
        try
        {
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

            ArrayList<String> paired = new ArrayList<>();

            for (BluetoothDevice bt : pairedDevices) {
                paired.add(bt.getName());

            }
            if (paired.size() > 0) {
                for (BluetoothDevice bt : pairedDevices) {
                    if (bt.getName().equals("Foodora BT Printer")) {
                        bluetoothDevice = bt;
                        service.setText(bt.getName()+" is attached");
                        break;
                    }

                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private int getcheck() {
        database = FirebaseDatabase.getInstance();
        getSku = database.getReference();

        getSku.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                int l = 0;
                j = 0;

                for(DataSnapshot ds :dataSnapshot.getChildren())
                {

                    List list = ds.getValue(List.class);
                    String sku = list.getSku();
                    l = Integer.parseInt(sku.toString().trim());
                    if(j<l)
                    {
                        j = l;
                    }
                }


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return j+1;

    }

    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
    }

    void openBluetoothPrinter()
    {
        try {
            UUID uuidString = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidString);
            bluetoothSocket.connect();
            outputStream = bluetoothSocket.getOutputStream();
            inputStream = bluetoothSocket.getInputStream();
           service.setText("Ready to use the printer");

            beginListenData();
        }
       catch(Exception ex){
           ex.printStackTrace();

        }


    }
//
    void beginListenData()
    {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            thread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = inputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                inputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;


                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            thread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sendData(String sku, int length , int width , String selectedColour) throws IOException
    {
        try {
            String msg;
                    // Printing the data

            msg = "SKU : "+sku ;

            msg += " L : "+Integer.toString(length) + " W : "+ Integer.toString(width);

            msg += "\n";

            outputStream.write(msg.getBytes());

            Toast.makeText(add.this, "Data Sent to printing", Toast.LENGTH_SHORT).show();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

