package com.example.list;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class take extends AppCompatActivity {
    EditText skuCode;
    FirebaseDatabase database;
    DatabaseReference ref , deleteSku;
    Button delete ;
    Button back;
    List sheets;
    ArrayList<String> li;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take);

        skuCode = (EditText) findViewById(R.id.editText3);
        delete = (Button) findViewById(R.id.button);
        back = (Button) findViewById(R.id.button5);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(take.this , homePage.class );
                startActivity(home);
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sheets = new List();
                li = new ArrayList<>();
                database = FirebaseDatabase.getInstance();
                ref = database.getReference();
                s = skuCode.getText().toString().trim();
                ref.addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        for(DataSnapshot ds :dataSnapshot.getChildren())
                        {
                            List list = ds.getValue(List.class);
                            if(list.getSku().equals(s))
                            {
                                    String val = (String)ds.getKey();
                                    deleteValue(val);
                            }

                        }

                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

    }

    private void deleteValue(String val) {
        database = FirebaseDatabase.getInstance();
        deleteSku = database.getReference().child(val);
        deleteSku.removeValue();

        Toast.makeText( take.this,"Item Removed",Toast.LENGTH_LONG).show();

    }


}
