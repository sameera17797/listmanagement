package com.example.list;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class homePage extends AppCompatActivity {
    private Button add;
    private Button take;
    private Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        add = (Button) findViewById(R.id.button7);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addpage();
            }
        });
        take = (Button) findViewById(R.id.button10);
        take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takepage();
            }
        });

        search = (Button) findViewById(R.id.button8);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchpage();
            }
        });



    }

    public void addpage(){
        Intent intent = new Intent(this ,add.class);
        startActivity(intent);
    }
    public void takepage()
    {
        Intent intent = new Intent(this ,take.class);
        startActivity(intent);
    }
    public void searchpage()
    {
        Intent intent = new Intent(this ,search2.class);
        startActivity(intent);
    }

}
