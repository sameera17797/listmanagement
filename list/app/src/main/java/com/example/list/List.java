package com.example.list;


import java.nio.charset.IllegalCharsetNameException;

public class List {
    private int len;
    private int wid;
    private String color, sku;


    public List() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public List(int len, int wid, String sku , String color) {
        this.len = len;
        this.wid = wid;
        this.color = color;
        this.sku = sku;

    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }


}
